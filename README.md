Clé de réemploi Emmabuntüs
==========

L’intérêt d’une telle clé est de permettre **de reconditionner en masse** des ordinateurs très facilement et rapidement – **de l’ordre de 2 à 10 minutes** – en y clonant de manière semi-automatique ou automatique soit un système GNU/Linux pré-configuré que nous fournissons via cette **[page](https://emmabuntus.org/telecharger-la-cle-usb-de-reemploi/)**, soit votre propre système que vous pouvez créer en suivant le tutoriel pour la réalisation d’un clone compatible avec notre clé.

Cette méthode utilise le logiciel [Clonezilla](https://fr.wikipedia.org/wiki/Clonezilla) pour effectuer le clonage, et l’utilitaire [Ventoy](https://www.ventoy.net/) pour la mise en œuvre de la clé de réemploi qui est réalisable sur Windows et Linux.

![Ventoy accueil thème réemploi](./documentation/Francais/Ventoy_accueil_theme_reemploi_cadre.png)

<details><summary>Vidéos de présentation de la clé (cliquez pour développer)</summary>

&nbsp;

<div align="center">
<a href="http://www.youtube.com/watch?feature=player_embedded&v=-Z_Srv5xjlU
" target="_blank"><img src="http://img.youtube.com/vi/-Z_Srv5xjlU/maxresdefault.jpg"
alt="Moins de trois minutes pour l'installation d'un ordinateur ❗ Qui dit mieux ?" width="640" /></a>
</div>

**<div align="center">![Moins de trois minutes pour l'installation d'un ordinateur ❗ Qui dit mieux ?](https://youtu.be/-Z_Srv5xjlU)</div>**
&nbsp;

<div align="center">
<a href="http://www.youtube.com/watch?feature=player_embedded&v=28McITZUTcQ
" target="_blank"><img src="http://img.youtube.com/vi/28McITZUTcQ/maxresdefault.jpg"
alt="Clé USB de réemploi 2.0 avec Ventoy - Utilisation du mode semi-automatique" width="640" /></a>
</div>

**<div align="center">![Clé USB de réemploi 2.0 avec Ventoy - Utilisation du mode semi-automatique](https://video.tedomum.net/w/8FnqiKo1okn4Y6BzCvTQH6)</div>**
&nbsp;

<div align="center">
<a href="http://www.youtube.com/watch?feature=player_embedded&v=QIY4FhMiLI0
" target="_blank"><img src="http://img.youtube.com/vi/QIY4FhMiLI0/maxresdefault.jpg"
alt="Clé USB de réemploi 2.0 avec Ventoy - Utilisation du mode semi-automatique)" width="640" /></a>
</div>

**<div align="center">![Clé USB  de réemploi 2.0 avec Ventoy - Utilisation du mode automatique/semi-automatique](https://video.tedomum.net/w/qJSUM2GLQh9DoQTq7H1NQR)</div>**
&nbsp;

<div align="center">
<a href="http://www.youtube.com/watch?feature=player_embedded&v=DU-mNZeEiQw
" target="_blank"><img src="http://img.youtube.com/vi/DU-mNZeEiQw/maxresdefault.jpg"
alt="Clé USB de réemploi 2.0 Ventoy - Création sur Windows et Linux" width="640" /></a>
</div>

**<div align="center">![Clé USB de réemploi 2.0 Ventoy - Création sur Windows et Linux](https://video.tedomum.net/w/qyQ3y6rUdrveeGUwFWEZjK)</div>**
&nbsp;

<div align="center">
<a href="http://www.youtube.com/watch?feature=player_embedded&v=-o60n4PzWtw
" target="_blank"><img src="http://img.youtube.com/vi/-o60n4PzWtw/maxresdefault.jpg"
alt="Clé USB de réemploi 2.0 Ventoy - Création évolution pour le mode automatique" width="640" /></a>
</div>

**<div align="center">![Clé USB de réemploi 2.0 Ventoy - Création évolution pour le mode automatique](https://video.tedomum.net/w/ejcobAk92AYq8REnHG2stT)</div>**
&nbsp;

<div align="center">
<a href="http://www.youtube.com/watch?feature=player_embedded&v=j0lwxCVy27U
" target="_blank"><img src="http://img.youtube.com/vi/j0lwxCVy27U/maxresdefault.jpg"
alt="Clé USB de réemploi 2.0 Ventoy - Création des clones" width="640" /></a>
</div>

**<div align="center">![Clé USB de réemploi 2.0 Ventoy - Création des clones](https://video.tedomum.net/w/c92YLQt8RpgAvfj8mDB7oC)</div>**
&nbsp;

</details>

Cette clé permet les opérations suivantes :
- Clonage semi-automatique en mode Legacy, UEFI ou Secure Boot avec choix du clone par l’opérateur ;
- Clonage automatique suivant 4 clones définis dans le fichier clone.ini ;
- Sauvegarde automatique du disque dur de référence et contrôle de la compatibilité de celui-ci avec nos schémas de partitionnement ;
- [Effacement sécurisé](https://fr.wikipedia.org/wiki/Effacement_de_donn%C3%A9es) des disques grâce à :
    - l’utilitaire [Nwipe](https://en.wikipedia.org/wiki/Nwipe) pour les disques magnétiques
    - [Hdparm](https://fr.wikipedia.org/wiki/Hdparm) pour les disques [SSD](https://fr.wikipedia.org/wiki/SSD)
    - [nvme-cli](https://github.com/linux-nvme/nvme-cli) pour les périphériques [NVMe](https://fr.wikipedia.org/wiki/NVM_Express)

Pour réaliser cette clé de remploi vous avez besoin des fichiers suivants :
- les scripts suivants contenus dans le dossier refurbishing_scripts sont à copier à la racine la partition « IMAGES » :
    - clone.sh : permet le clonage semi-automatique et automatique
    - parted.sh : permet de partitionner le disque dur cible et contient les schémas de partitionnement
    - save_clone.sh : permet la sauvegarde du clone en automatique et de contrôler la compatibilité du schéma de partitionnement du disque dur de référence
    - erase.sh : permet l’effacement sécurisé du disque dur magnétique avec le choix du mode d’effacement : Remplissage de zéro,  DoD court, [DoD 5220.22M](https://en.wikipedia.org/wiki/National_Industrial_Security_Program#NISP_Operating_Manual_(DoD_5220.22-M)), et autre choix défini par l’utilisateur dans le [TUI](https://fr.wikipedia.org/wiki/Environnement_en_mode_texte) de Nwipe. Pour les disques SSD Hdparm est utilisé, et pour les périphériques NVMe, c’est nvme-cli  qui est utilisé
    - clone.ini : fichier de configuration du clonage en mode automatique
- ventoy.zip : contient la configuration de Ventoy pour permettre le lancement des scripts à décompresser dans la partition « Ventoy » ;
- une ou plusieurs ISO de Clonezilla : version 64 bits pour les ordinateurs 64 bits, UEFI et Secure Boot, et 32 bits pour les ordinateurs 32 bits uniquement ;
- un ou plusieurs clones compatibles avec notre clé de réemploi.

Les fichiers nécessaires pour la clé de réemploi sont disponibles sur :
- Sources : [http://usb-reemploi.emmabuntus.org](http://usb-reemploi.emmabuntus.org)
- Clones :  [https://mirrors.o2switch.fr/emmabuntus/Clones/](https://mirrors.o2switch.fr/emmabuntus/Clones/) ou [http://anyathome.be/](http://anyathome.be/)

Pour plus d’informations, voir les tutoriels suivants :
- [Utilisation des clés USB de réemploi Emmabuntüs](https://emmabuntus.org/utilisation-de-la-cle-usb-de-reemploi-emmabuntus/)
- [Utilisation de la clé USB de réemploi Emmabuntüs avec le « Secure Boot »](https://emmabuntus.org/utilisation-de-la-cle-usb-de-reemploi-emmabuntus-avec-le-secure-boot/)
- [Réalisation de la clé USB de réemploi Emmabuntüs avec Ventoy pour Windows et Linux](https://emmabuntus.org/realisation-de-la-cle-usb-de-reemploi-emmabuntus-sous-ventoy/)
- [Méthode de réalisation de la clé de réemploi Emmabuntüs sous MultiSystem pour Linux](https://emmabuntus.org/methode-de-realisation-de-la-cle-de-reemploi-emmabuntus/)
- [Réalisation d’un clone pour la clé USB de réemploi Emmabuntüs](https://emmabuntus.org/realisation-dun-clone-pour-la-cle-usb-de-reemploi-emmabuntus/)


Ce logiciel est diffusé sous licence libre GPL3.0 par le [collectif Emmabuntüs](https://emmabuntus.org/).


----------

Emmabuntüs refurbishing key
==========

The interest of such a USB flash drive device, that we call here “the refurbishing key” is to allow **mass reconditioning** of computers through an **easy and quick process, in the order of 2 to 10 minutes**. This is achieved by cloning in a **semi-automatic or automatic way**, either a pre-configured GNU/Linux system that we provide via this page, or your own system that you can create by following the tutorial for the making of a clone compatible with our key.

This method uses the Clonezilla software to perform the cloning operation, and the Ventoy utility to prepare the refurbishing key which is feasible on both Windows and Linux.

This key allows the following operations:
- Semi-automatic cloning in Legacy, UEFI or Secure Boot mode with the choice of clone left to the operator;
- Automatic cloning according to 4 clones defined in the clone.ini file;
- Automatic backup and compatibility check of the reference hard disk with our partitioning schemes;
- [Secure erase](https://en.wikipedia.org/wiki/Data_erasure) : allows you to securely erase the target hard drive using :
    - [Nwipe](https://en.wikipedia.org/wiki/Nwipe) utility for magnetic disks
    - [HDparm](https://en.wikipedia.org/wiki/Hdparm) for Solid-State Drives
    - [nvme-cli](https://github.com/linux-nvme/nvme-cli) for NVMe devices

To make this refurbishing key you need the following files:
- the following scripts contained in the refurbishing_scripts folder are to be copied to the root of the "IMAGES" partition :
   - clone.sh : allows the cloning in semi-automatic and automatic modes
   - parted.sh : is used to partition the target hard disk and contains the partitioning schemes
   - save_clone.sh : performs the automatic backup of the clone and checks the compatibility of the partitioning scheme of the reference hard disk
   - erase.sh : allows you to securely erase the target hard drive using the Nwipe utility for magnetic disks, with the choice of erase mode: Zero Fill, Short DoD, DoD [DoD 5220.22M](https://en.wikipedia.org/wiki/National_Industrial_Security_Program#NISP_Operating_Manual_(DoD_5220.22-M)), and another user-defined choice in the Nwipe [TUI](https://en.wikipedia.org/wiki/Text-based_user_interface). Hdparm is used for Solid-State Drives and nvme-cli for NVMe devices.
   - clone.ini : configuration file for the cloning in  automatic mode
- ventoy.zip : contains the Ventoy configuration to allow the launching of the scripts to decompress in the "Ventoy" partition;
- one or more Clonezilla ISOs: 64-bit version for 64-bit computers, UEFI and Secure Boot, and 32-bit for 32-bit computers only;
- one or more clones compatible with our refurbishing key.

The files needed for the refurbishing key are available on these pages :
- Sources : [http://usb-reemploi.emmabuntus.org](http://usb-reemploi.emmabuntus.org)
- Clones  :  :  [https://mirrors.o2switch.fr/emmabuntus/Clones/](https://mirrors.o2switch.fr/emmabuntus/Clones/) or [http://anyathome.be/](http://anyathome.be/)

For more information , you can look at the following tutorials :
- [Using Emmabuntüs refurbishing USB keys](https://emmabuntus.org/using-emmabuntus-refurbishing-usb-keys/)
- [How to make an Emmabuntus refurbishing key with Ventoy for Windows and Linux](https://emmabuntus.org/how-to-make-an-emmabuntus-refurbishing-key-with-ventoy/)
- [How-to make an Emmabuntüs refurbishing key on MultiSystem for Linux](https://emmabuntus.org/how-to-make-an-emmabuntus-refurbishing-key/)
- [Making a clone to be used with the Emmabuntus refurbishing key](https://emmabuntus.org/making-a-clone-to-be-used-with-the-emmabuntus-refurbishing-key/)

This software is released by the [Emmabuntüs collective](https://emmabuntus.org/) under the GPL3.0 free license.
